pipeline {
    agent any
    tools {
        // * Se define la herramienta de Maven para la compilación del proyecto como instrucción dentro de la Pipeline.
        // * Asociando la tarea a la configuración de Maven en jenkins.
        maven 'mavenTool'
    }
    // * agregar la variable antes de la definición de las stages
    environment {
        PROJECT_ROOT = '.'
        PROJECT_JAR_NAME = 'pragma-monolitico-0.0.1-SNAPSHOT'
        REGISTRY = 'santiagoposada/pragma-monolitico-jsq'
        REGISTRY_CREDENTIAL = 'dockerhub'
        DOCKERHUB_USERNAME = 'santiagoposada'
        DOCKERHUB_PASS = '**********'
    }
    stages {
        stage('checkout') {
            steps {
                // * Se creará el script con las credenciales de acceso al
                // * repositorio de Git dónde esta el código fuente del proyecto
                // * y el Pipeline sea capaz de obtener el mismo para empezar a ejecutar tareas.
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [],
                userRemoteConfigs: [[credentialsId: 'pragma-gitlab',
                url: 'https://gitlab.com/santiago.posada/api-pragma-monolitico-docker.git']]])
            }
        }
        // * Se define la etapa de compilación del proyecto para el artefacto proporcionado, de tipo Maven.
        stage('build') {
            steps {
                // ! -DskipTests es para no ejectutar pruebas unitarias
                sh 'mvn clean install -DskipTests'
            }
        }
        // TODO: Con este último paso se concluye la de definición de la etapa de
        // * construcción del artefacto proporcionado desde el Pipeline y se prueba ejecutando el Pipeline.
        // * Finalmente, vamos a escribir el código que dará a entender al
        // * pipeline la ejecución de esta nueva etapa y el análisis en sonar.
        stage('scan') {
            environment {
                SACANNER_HOME = tool 'sonar-scanner'
            }
            steps {
                withSonarQubeEnv('mysonarqube') {
                    sh "${SACANNER_HOME}/bin/sonar-scanner \
                    -Dsonar.projectKey=pragma-monolitico:Test \
                    -Dsonar.projectName=pragma-monolitico  \
                    -Dsonar.projectVersion=0.${BUILD_NUMBER} \
                    -Dsonar.sources=${PROJECT_ROOT}/src/main \
                    -Dsonar.language=java \
                    -Dsonar.java.binaries=./${PROJECT_ROOT}/target/classes \
                    -Dsonar.java.test.binaries=${PROJECT_ROOT}/src/test/java \
                    -Dsonar.junit.reportPaths=./${PROJECT_ROOT}/target/surefire-reports \
                    -Dsonar.coverage.jacoco.xmlReportPaths=./${PROJECT_ROOT}/target/site/jacoco/jacoco.xml \
                    -Dsonar.java.coveragePlugin=jacoco"
                }
            }
        }
        // TODO: continuaremos con la construcción de la etapa de despliegue,
        //*que en conjunto con el stage de construcción y análisis conforman el flujo básico y principal de un pipeline.
        stage('build-image') {
            steps {
                sh 'echo ------ BUILDING DOCKER-IMAGE ----'
                // Move the generated "jar" artifact to the root path
                sh "cd ${PROJECT_ROOT}/target;"
                sh "docker build -t ${REGISTRY}:${BUILD_NUMBER} . "
            }
        }
        stage('deploy-image') {
            steps {
                sh 'echo ---- PUSHING IMAGE TO DOCKERHUB ----'
                //I If the Dockerhub authentication stopped, do it again
                //Remember to enter the Jenkins" Docker container and do it manually
                sh "docker login --username ${DOCKERHUB_USERNAME} --password ${DOCKERHUB_PASS}"
                sh "docker push ${REGISTRY}:${BUILD_NUMBER}"
            }
        }
    }
}
